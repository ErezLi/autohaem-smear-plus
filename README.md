# autohaem smear+

A mechanical and electronic 3D printed device for automated blood smears.

![Photo of autohaem smear](images/autohaem%20smear+%20title%20card.jpg)
## Printing and building it

For prebuilt STLs and assembly instructions, go to the [autohaem smear+ webpage](https://autohaem.org/smear+/)

## Software

autohaem smear+ is controlled using the [autohaem smear+ arduino code](https://gitlab.com/autohaem/autohaem-smear-plus-arduino).  

## Customising it

All of the designs are made using [OpenSCAD](https://www.openscad.org/). Currently we use [OpenSCAD 2019.05](https://openscad.org/news.html#20190518).

### Cloning the repository

The autohaem smear+ repository uses additional git features which can require extra steps. For this reason, it is recommended that you clone it, rather than download it.  

* The repository uses [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to include parameters, objects and components from the [autohaem smear repository](https://gitlab.com/autohaem/autohaem-smear).

* The repository uses [git-lfs](https://git-lfs.github.com/) for version control of image (and other large) files. If git-lfs is not installed, cloning the repository will only download placeholders of these files.

#### Visual Studio Code

The easiest way to clone the repository is with [Visual Studio (VS) Code](https://code.visualstudio.com/), with [git-lfs installed](https://git-lfs.github.com/).  If you [clone the repository using VS Code](https://code.visualstudio.com/docs/editor/versioncontrol#_cloning-a-repository) these additional features should work automatically.

You can then make changes using VS Code and use OpenScad with the editor hidden and the `automatic reload and compile` option ticked for compiling.

#### Cloning using the command line

1. Download and install [git-lfs](https://git-lfs.github.com/).

2. The easiest way to correctly clone the project is to run:

    ```bash
    git clone --recurse-submodules https://gitlab.com/autohaem/autohaem-smear-plus.git
    ```

    If the `autohaem smear` folder is empty, you should run the following in the main `autohaem-smear-plus` folder:
    1. `git submodule init` (to set the repository up for the first time).
    2. `git submodule update` (to fetch the required data).

 > With git-lfs installed, git will download the latest version of the large files.  If they are still missing run:

```bash
git lfs fetch 
git lfs checkout
```

  > To make Git always download everything in the repository run the following commands in your terminal:

```bash
git config --local lfs.fetchexclude ""
git lfs fetch
git lfs checkout
```

### Downloading the repository

1. Download the `autohaem-smear-plus` repository.
2. In the autohaem-smear-plus repository on GitLab, click on the 8 characters (the commit ref) after the `@` on the folder named `autohaem-smear @ ...`. This will take you to the autohaem-smear repository at the correct commit.
     > :warning: It is important that the submodule is at the correct commit, it will not work if it is on a different commit.
3. Download the autohaem-smear repository and copy these files in the folder called `autohaem-smear` in the autohaem smear+ folder.

### Guide to repository

#### autohaem smear+ components

The autohaem smear+ components are found in the `openscad` folder. For example the base is compiled from `openscad/base.scad`. Some parts of the design borrow objects from the `autohaem-smear` submodule.

#### OpenFlexure Microscope components

The `autohaem-smear` folder is a submodule containing a version of the [autohaem smear repository](https://gitlab.com/autohaem/autohaem-smear) at a specific commit. The components common to both devices are compiled from there.  If you make changes to these components that you feel should be merged in for others to use, then you should make a merge request to that repository (bearing in mind that it will be ahead of the specific commit that this repository uses).

The shared parameters for both devices reside in `autohaem-smear/openscad/shared_parameters.scad`. The parameters for `autohaem smear` reside in `/openscad/smear_plus_parameters.scad`.

#### Building the components

To build every component you can run ``build.sh`` if you have a Bash shell (in either linux or Windows Subsystem Linux).  You may need to add ``openscad`` to your path if you want this to work in MinGW on Windows, or in the Mac OS Terminal.

It is also useful to look in ``build.sh`` to see how the components are compiled (and from which OpenSCAD file).

To build individual components on the command line, you can run the following:

```bash
openscad -o builds/base.stl openscad/base.scad
```
