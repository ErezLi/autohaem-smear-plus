#!/bin/bash

mkdir -p builds

openscad -o builds/slider_motor.stl openscad/slider_motor.scad
openscad -o builds/smear_plus_main_body.stl openscad/smear_plus_main_body.scad
openscad -o builds/base.stl openscad/base.scad
openscad -o builds/limit_switch_lid.stl openscad/limit_switch_lid.scad
