# Arduino power supply

Any suitable power supply for an Arduino Uno.

* 2.1mm center-positive DC Barrel Jack
* Recommended 9V to 12V
  
Available from e.g. [Amazon](https://www.amazon.co.uk/RockJam-Keyboard-RJ654，Arduino-Roberts-SRS-XB40/dp/B08SC7JC87)
