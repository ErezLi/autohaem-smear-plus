# Prepare Electronics

{{BOM}}

[3.2mm heatshrink]: "{cat:part}"
[solder]: "{cat:part}"
[breadboard jumper wire]: "{cat:part}"
[soldering iron]: "{cat:tool}"
[button]: components/LED_button.md "{cat:part}"
[wire stripper]: "{cat:tool}"
[craft knife]: "{cat:tool}"
[heat gun]: "{cat:tool}"
[wire cutters]: "{cat:tool}"
[100$\Omega$ resistor]: "{cat:part}"
[8mm diameter LED pushbutton]: components/LED_button.md "{cat:part}"
[limit switch]: components/limit_switch.md "{cat:part}"
[DC socket]: components/DC_socket.md#socket "{cat:part}"
[4A instrument wire]: "{cat:part}"

# Method

## Solder the LED pushbutton {pagestep}

Referring to the diagram below, solder the [8mm diameter LED pushbutton]{qty:1} to four pieces of [breadboard jumper wire]{qty:45cm} using some [solder]{qty:some}, [soldering iron]{qty:1}, [wire cutter]{qty:1} and [wire stripper]{qty:1}. For the positive LED terminal, connect it to a [100$\Omega$ resistor]{qty:1}. Use [3.2mm heatshrink]{qty:5cm} to insulate the connections.

![](images/prepare_electronics/LED.jpg)

You have now successfully built the [soldered LED pushbutton]{output, Qty:1}. You will need this later.

## Solder the limit switch {pagestep}

Solder two pieces of [4A instrument wire]{qty:8cm} to each end terminal of the [limit switch]{qty:1} using [solder]{qty:some} and a [soldering iron]{qty:1}.

![](images/prepare_electronics/sensor.jpg)

You have now successfully built the [soldered limit switch]{output, Qty:1}. You will need this later.

## Solder the DC socket {pagestep}

Referring to the diagram below, solder the terminals of the [DC socket]{qty:1} using [4A instrument wire]{qty:4cm} using [solder]{qty:some} and a [soldering iron]{qty:1}.


![](images/prepare_electronics/soldered_socket.jpg)


You have now successfully built the [soldered DC socket]{output,qty:1}. You will need this later.
