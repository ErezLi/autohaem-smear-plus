include <./smear_plus_parameters.scad>;
use <../autohaem-smear/openscad/utilities.scad>;
use <../autohaem-smear/openscad/slider.scad>;



module rod_bridge(){
    translate([0,slider_size.y/2+slider_linear_bearing_box_width/2,0])cube([slider_linear_bearing_box_length,slider_linear_bearing_box_width,slider_size.z],center=true);
}

module rod_bridge_holes(){
    translate([0,distance_between_rods/2,-2])rotate([0,90,0])cube([10,10,100], center=true);
}

module slider_motor_nema_main(){
        smear_plus_slider();
}

module clamp_base(){
    translate([-(clamp_position+2),0,0,])cube([clamp_position+2,slider_size.z/2,slider_size.z],center = true);
}

module nema_motor_nut_holder(){
    difference(){
        hull(){
            translate([0,-3,0])cube([12,28.5,14]);
            translate([0,44.5,0])cube([12,tiny(),slider_size.z]);
        }
        translate([-50,25/2-1,11/2])rotate(-90)cylinder_with_45deg_top(r=11/2,h=100,$fn=100);
        translate([-50,25/2-11/2-1,0])cube([100,2*11/2,11/2]);
        translate([12/2-4.5/2,25/2-24/2-1,0])cube([4.5,24,100]);
        translate([-50,25/2-8-1,11/2])rotate([0,0,-90])cylinder_with_45deg_top(r=3.2/2,h=100, $fn=100);
        translate([-50,25/2+8-1,11/2])rotate([0,0,-90])cylinder_with_45deg_top(r=3.2/2,h=100, $fn=100);

    }
}

module clamp_motor_slider(){
    difference(){
    union(){
        slider_motor_nema_main();
        translate([-12/2,-25/2-box_size.y/2-nema_17_size.y/2,-11/2])nema_motor_nut_holder();
        //clamp_base();
        //   translate([clamp_centre+clamp_position,0,6])clamp_holder();
        //   translate([clamp_centre-clamp_position,0,6])rotate([0,0,180])clamp_holder();
    }
    slide();
    translate([slider_size.x/2-4,slider_size.y/2,0])rotate([90,0,0])trylinder_selftap(nominal_d = 3 , h=5);
    translate([-(slider_size.x/2-4),slider_size.y/2,0])rotate([90,0,0])trylinder_selftap(nominal_d = 3 , h=5);
    translate([slider_size.x/2,0,0])rotate([0,-90,0])rotate([0,0,25])trylinder_selftap(nominal_d=3,h=7);
    }
}

module slider_magnet(){
    difference(){
            union(){
            difference(){
                union(){
                    rod_bridge();
                    translate([0,slider_size.y/2-4.5,0])cube([slider_size.x,9.5,slider_size.z],center=true);
                }
                reflect([1,0,0])translate([slider_size.x/2-4,12,0])rotate([0,180,0])screw_y(d=1.5*2,h=3,$fn=50,shaft=true);
                rod_bridge_holes();
            }
                
        }    
    }
}

clamp_motor_slider();
rotate([180,0,0])translate([0,-60,0]) slider_magnet();



